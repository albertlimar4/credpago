<?php

use Credpago\Model;
use Credpago\Model\Site;
use Credpago\Model\SiteRequest;
use Credpago\Model\User;
use Credpago\Page;
use Slim\Slim;

require_once("vendor/autoload.php");
require_once("functions.php");

$app = new Slim();

$app->config('debug', true);

/**
 * Página inicial do sistema
 */
$app->get('/', function() {
    User::verifyLogin();
    $nUseId = $_SESSION[User::SESSION]["UseId"];

    $oSite = new Site();

    $page = new Page();

    $page->setTpl("index", [
        "nSite"=>count($oSite->listAllFromUser($nUseId))
    ]);

});

/**
 * Formulário de Login
 */
$app->get('/login', function() {

    $page = new Page([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("login", [
        "vError"=>Model::getError()
    ]);

});

/**
 * Processamento de Login
 */
$app->post('/login', function() {

    try{
        $oUser = new User();
        $oUser->setUseEmail(validateInput($_POST['UseEmail']));
        $oUser->login();
        header("Location: /");
        die();
    } catch (Exception $e) {
        Model::setError($e->getMessage());
        header("Location: /login");
        die();
    }

});

/**
 * Processamento de logout
 */
$app->get('/logout', function() {

    User::logout();

    header("Location: /login");
    exit();

});

/**
 * Formulário de Cadastro de usuários
 */
$app->get('/cadastro', function() {

    $page = new Page([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("register", [
        "vError"=>Model::getError()
    ]);

});

/**
 * Processamento de cadastro de usuários
 */
$app->post('/cadastro', function() {

    try{

        $oUser = new User();
        $oUser->setUseName(validateInput($_POST['UseNome']));
        $oUser->setUseEmail(validateInput($_POST['UseEmail']));
        $oUser->setUsePassword($_POST['UsePassword']);
        if ($oUser->save())
            header("Location: /login");
        else
            header("Location: /cadastro");
        die();

    } catch (Exception $e) {

        Model::setError($e->getMessage());
        header("Location: /cadastro");
        die();

    }

});

/**
 * Listagem de URLs
 */
$app->get('/site/list', function() {
    User::verifyLogin();

    $oSite = new Site();
    $vSite = $oSite->listAllFromUser($_SESSION[User::SESSION]["UseId"]);

    $oSiteRequest = new SiteRequest();
    $vRequest = [];

    foreach ($vSite as $Site) {
        $vRequest[$Site['sit_id']] = $oSiteRequest->listAllFromSite($Site['sit_id']);
    }

    $page = new Page([
        'header'=>false,
        'footer'=>false
    ]);
    $page->setTpl("site-list", [
       "vSite"=>$vSite,
        "vRequest"=>$vRequest
    ]);

});

/**
 * Formulário de cadastro de URLs
 */
$app->get('/site/create', function() {
    User::verifyLogin();

    $page = new Page([
        'header'=>false,
        'footer'=>false
    ]);
    $page->setTpl("site-form");

});

/**
 * Formulário de cadastro de URLs
 */
$app->get('/site/:nSitId/update', function($nSitId) {
    User::verifyLogin();

    $oSite = new Site();

    $page = new Page([
        'header'=>false,
        'footer'=>false
    ]);
    $page->setTpl("site-form-update", [
        "vSite"=>$oSite->get($nSitId)
    ]);

});

/**
 * Deletar URL
 */
$app->get('/site/:nSitId/delete', function($nSitId) {
    User::verifyLogin();

    $oSite = new Site();
    $oSite->setSitId($nSitId);
    $oSite->delete();

});

/**
 * Processamento de cadastro de URL
 */
$app->post('/site', function() {
    User::verifyLogin();

    $oSite = new Site();
    $oSite->setSitName(validateInput($_POST['SitName']));
    $oSite->setSitUrl(validateInput($_POST['SitUrl']));
    $oSite->setUseId($_SESSION[User::SESSION]["UseId"]);
    $oSite->save();
});

/**
 * Processamento de atualização de URL
 */
$app->post('/site/update', function() {
    User::verifyLogin();

    $oSite = new Site();
    $oSite->setSitId(validateInput($_POST['SitId']));
    $oSite->setSitName(validateInput($_POST['SitName']));
    $oSite->setSitUrl(validateInput($_POST['SitUrl']));
    $oSite->setUseId($_SESSION[User::SESSION]["UseId"]);
    $oSite->update();
});

/**
 * Processamento de requisições de URL
 */
$app->get('/requisicao', function() {
    $oSite = new Site();
    $vSite = $oSite->listAll();

    $oSiteRequest = new SiteRequest();
    $oSite = new Site();

    foreach ($vSite as $Site) {
        // Create a curl handle
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Site['sit_url']);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_NOBODY  , false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        // execute
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        //Salvar informações da requisição

        $oSiteRequest->setReqCode($info['http_code']);
        $oSiteRequest->setSitId($Site['sit_id']);

        $oSite->setSitId($Site['sit_id']);
        $oSite->setSitBody($result);

        if ($oSiteRequest->save()) {
            if ($oSite->updateBody())
                echo json_encode(['Feito']);
            else
                echo json_encode(['Erro']);
        }
        else
            echo json_encode(['Erro']);
    }

});

/**
 * Lista de requisições por URL
 */
$app->get('/requisicao/:nSitId/lista', function($nSitId) {
    User::verifyLogin();

    $oSiteRequest = new SiteRequest();

    $page = new Page([
        "header"=>false,
        "footer"=>false
    ]);

    $page->setTpl("request-list", [
        "vRequest"=>$oSiteRequest->listAllFromSite($nSitId)
    ]);

});

$app->run();