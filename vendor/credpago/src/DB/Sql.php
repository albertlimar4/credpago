<?php

namespace Credpago\DB;

use PDOException;
use PDO;

class Sql {

    const HOSTNAME_LC = "localhost:3306";
    const USERNAME_LC = "root";
    const PASSWORD_LC = "";
    const DBNAME_LC = "credpago";

    private $conn;

    public function __construct()
    {

        try {
            $this->conn = new PDO(
                "mysql:dbname=".Sql::DBNAME_LC.";host=".Sql::HOSTNAME_LC.";charset=utf8",
                Sql::USERNAME_LC,
                Sql::PASSWORD_LC,
                array(
                    PDO::ATTR_PERSISTENT => true
                )
            );
        } catch (PDOException $e) {
            throw new PDOException($e);
        }

    }

    private function setParams($statement, $parameters = array())
    {

        foreach ($parameters as $key => $value) {

            $this->bindParam($statement, $key, $value);

        }

    }

    private function bindParam($statement, $key, $value)
    {

        $statement->bindParam($key, $value);

    }

    public function query($rawQuery, $vParam = array()): array
    {
//        echo "<hr>{$rawQuery}<hr>";
//        foreach ($vParam as $sParam) {
//            echo ($sParam) ? "'{$sParam}'," : "null,";
//        }

        $stmt = $this->conn->prepare($rawQuery);

        $this->setParams($stmt, $vParam);

        $stmt->execute();

        return $stmt->errorInfo();

    }

    public function select($rawQuery, $vParam = array()):array
    {

//        echo "<hr>{$rawQuery}<hr>";
//        foreach ($vParam as $sParam) {
//            echo ($sParam || $sParam == 0) ? "'{$sParam}'," : "null,";
//        }

        $stmt = $this->conn->prepare($rawQuery);

        $this->setParams($stmt, $vParam);

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

}