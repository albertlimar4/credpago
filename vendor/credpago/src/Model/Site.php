<?php

namespace Credpago\Model;

use Credpago\DB\Sql;
use Credpago\Model;
use Exception;

class Site extends Model
{
    private $SitId;
    private $SitName;
    private $SitUrl;
    private $SitBody;
    private $UseId;

    public function listAllFromUser($nUseId): array
    {
        $sql = new Sql();
        return $sql->select("Select * From site Where use_id = :nUseId", [
            ":nUseId"=>$nUseId
        ]);
    }

    public function listAll(): array
    {
        $sql = new Sql();
        return $sql->select("Select * From site");
    }

    public function recoverSiteUser($sSitUrl,$nUseId)
    {
        $sql = new Sql();
        $vSite = $sql->select("Select * From site Where sit_url = :sSitUrl And use_id = :nUseId", [
            ":sSitUrl"=>$sSitUrl,
            ":nUseId"=>$nUseId
        ]);

        return ($vSite) ? $vSite[0] : false;
    }

    public function get($nSitId)
    {
        $sql = new Sql();
        $vSite = $sql->select("Select * From site Where sit_id = :nSitId", [
            ":nSitId"=>$nSitId
        ]);

        if ($vSite)
            return $vSite[0];
    }

    public function save()
    {
        $sql = new Sql();

        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$this->getSitUrl())) {
            echo json_encode(['sMsg' => 'URL inválida!', 'sClass' => 'error']);
            die();
        }

        if ($this->recoverSiteUser($this->getSitUrl(),$this->getUseId())){
            echo json_encode(['sMsg'=>'URL já cadastrada anteriormente!','sClass'=>'error']);
            die();
        }

        $vResult = $sql->query("Insert Into site (sit_name, sit_url, use_id) Values (:sSitName, :sSitUrl, :nUseId)", [
            ":sSitName"=>$this->getSitName(),
            ":sSitUrl"=>$this->getSitUrl(),
            ":nUseId"=>$this->getUseId()
        ]);

        if ($vResult[0] == 0)
            echo json_encode(['sMsg'=>'Cadastro realizado com sucesso!','sClass'=>'success']);
        else
            echo json_encode(['sMsg'=>$vResult[0], 'sClass'=>'error']);
        die();
    }

    /**
     * @throws Exception
     */
    public function update()
    {
        $sql = new Sql();

        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$this->getSitUrl())) {
            echo json_encode(['sMsg' => 'URL inválida!', 'sClass' => 'error']);
            die();
        }

        $vSite = $this->recoverSiteUser($this->getSitUrl(),$this->getUseId());
        
        if ($vSite && $vSite['sit_id'] != $this->getSitId()){
            echo json_encode(['sMsg'=>'URL já cadastrada anteriormente!','sClass'=>'error']);
            die();
        }

        $vResult = $sql->query("Update site set sit_name = :sSitName, sit_url = :sSitUrl Where sit_id = :nSitId", [
            ":nSitId"=>$this->getSitId(),
            ":sSitName"=>$this->getSitName(),
            ":sSitUrl"=>$this->getSitUrl()
        ]);

        if ($vResult[0] == 0)
            echo json_encode(['sMsg'=>'Atualização realizada com sucesso!','sClass'=>'success']);
        else
            echo json_encode(['sMsg'=>$vResult[0], 'sClass'=>'error']);
        die();
    }

    public function delete()
    {
        $sql = new Sql();
        $vResult = $sql->query("Delete From site Where sit_id = :nSitId", [
            ":nSitId"=>$this->getSitId()
        ]);

        if ($vResult[0] == 0)
            echo json_encode(['sMsg'=>'URL apagada com sucesso!','sClass'=>'success']);
        else
            echo json_encode(['sMsg'=>$vResult[0], 'sClass'=>'error']);
        die();
    }

    public function updateBody()
    {
        $sql = new Sql();

        $vResult = $sql->query("Update site set sit_body = :sSitBody Where sit_id = :nSitId", [
            ":nSitId"=>$this->getSitId(),
            ":sSitBody"=>$this->getSitBody()
        ]);

        if ($vResult[0] == 0)
            return true;
        else
            return false;
    }

    /**
     * @return mixed
     */
    public function getSitId()
    {
        return $this->SitId;
    }

    /**
     * @param mixed $SitId
     */
    public function setSitId($SitId): void
    {
        $this->SitId = $SitId;
    }

    /**
     * @return mixed
     */
    public function getSitName()
    {
        return $this->SitName;
    }

    /**
     * @param mixed $SitName
     */
    public function setSitName($SitName): void
    {
        $this->SitName = $SitName;
    }

    /**
     * @return mixed
     */
    public function getSitUrl()
    {
        return $this->SitUrl;
    }

    /**
     * @param mixed $SitUrl
     */
    public function setSitUrl($SitUrl): void
    {
        $this->SitUrl = $SitUrl;
    }

    /**
     * @return mixed
     */
    public function getSitBody()
    {
        return $this->SitBody;
    }

    /**
     * @param mixed $SitUrl
     */
    public function setSitBody($SitBody): void
    {
        $this->SitBody = $SitBody;
    }

    /**
     * @return mixed
     */
    public function getUseId()
    {
        return $this->UseId;
    }

    /**
     * @param mixed $UseId
     */
    public function setUseId($UseId): void
    {
        $this->UseId = $UseId;
    }


}