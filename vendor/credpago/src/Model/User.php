<?php

namespace Credpago\Model;

use Credpago\DB\Sql;
use Credpago\Model;
use Exception;

class User extends Model
{

    const SESSION = "User";
    const SECRET = "CredPagoPhp7_Secret";

    private $UseId;
    private $UseName;
    private $UseEmail;
    private $UsePassword;
    private $UseRegister;

    private static function recoverUserEmail($sUseEmail)
    {
        $sql = new Sql();
        $vUser = $sql->select("Select * From user Where use_email = :sUseEmail", [
            ":sUseEmail"=>$sUseEmail
        ]);

        return ($vUser) ? $vUser[0] : false;
    }

    /**
     * @throws Exception
     */
    private static function validateRegister($sUseName, $sUseEmail): bool
    {

        if (empty($sUseName) || !preg_match("/^[a-zA-Z-' ]*$/",$sUseName))
            throw new Exception("Nome Inválido!");

        if (empty($sUseEmail) || !filter_var($sUseEmail, FILTER_VALIDATE_EMAIL))
            throw new Exception("E-mail inválido!");

        if (self::recoverUserEmail($sUseEmail))
            throw new Exception("E-mail já cadastrado anteriormente!");

        return true;
    }

    /**
     * @throws Exception
     */
    public function save(): bool
    {

        if (self::validateRegister($this->getUseName(),$this->getUseEmail())){

            $sUsePassword = password_hash($this->getUsePassword(), PASSWORD_DEFAULT, [
                "cost"=>12
            ]);

            $sql = new Sql();

            $vResult = $sql->query("Insert Into user (use_name, use_email, use_password) values (:sUseName, :sUseEmail, :sUsePassword)", array(
                ":sUseName"=>$this->getUseName(),
                ":sUseEmail"=>$this->getUseEmail(),
                ":sUsePassword"=>$sUsePassword
            ));

            if ($vResult[0] == 0){
                Model::setError("Cadastro realizado com sucesso!", "success");
                return true;
            } else
                throw new Exception($vResult[0]);

        } else
            return false;
    }

    /**
     * @throws Exception
     */
    public function login(){

        $sql = new Sql();

        $vUser = self::recoverUserEmail($this->getUseEmail());

        if (!$vUser)
            throw new Exception("Usuário não cadastrado!");

        $this->setData($vUser);

        if (password_verify($_POST['UsePassword'], $this->getUsePassword()) === true){
            if (!isset($_SESSION))session_start();

            $_SESSION[User::SESSION]['UseId'] = $this->getUseId();
            $_SESSION[User::SESSION]['UseName'] = $this->getUseName();
            $_SESSION[User::SESSION]['UseEmail'] = $this->getUseEmail();
            $_SESSION[User::SESSION]['UseRegister'] = $this->getUseRegister();
            $_SESSION[User::SESSION]['LastActivity'] = time();
            unset($_SESSION[User::SESSION]['UseSenha']);

            return true;
        } else
            throw new Exception("Senha inválida!");
    }

    public static function verifyLogin(): bool
    {

        if (!isset($_SESSION))session_start();

        if (
            !isset($_SESSION[User::SESSION])
            ||
            !$_SESSION[User::SESSION]
            ||
            !isset($_SESSION[User::SESSION]["UseId"])
            ||
            (time() - $_SESSION[User::SESSION]['LastActivity'] > (2*3600))
        ) {
            header("Location: /login");
            exit();

        } else {
            $_SESSION[User::SESSION]['LastActivity'] = time();
            return true;
        }

    }

    public static function logout(){

        if (!isset($_SESSION))session_start();

        $_SESSION[User::SESSION] = null;

        session_destroy();
    }

    /**
     * @return mixed
     */
    public function getUseId()
    {
        return $this->UseId;
    }

    /**
     * @param mixed $UseId
     */
    public function setUseId($UseId): void
    {
        $this->UseId = $UseId;
    }

    /**
     * @return mixed
     */
    public function getUseName()
    {
        return $this->UseName;
    }

    /**
     * @param mixed $UseName
     */
    public function setUseName($UseName): void
    {
        $this->UseName = $UseName;
    }

    /**
     * @return mixed
     */
    public function getUseEmail()
    {
        return $this->UseEmail;
    }

    /**
     * @param mixed $UseEmail
     */
    public function setUseEmail($UseEmail): void
    {
        $this->UseEmail = $UseEmail;
    }

    /**
     * @return mixed
     */
    public function getUsePassword()
    {
        return $this->UsePassword;
    }

    /**
     * @param mixed $UsePassword
     */
    public function setUsePassword($UsePassword): void
    {
        $this->UsePassword = $UsePassword;
    }

    /**
     * @return mixed
     */
    public function getUseRegister()
    {
        return $this->UseRegister;
    }

    /**
     * @param mixed $UseRegister
     */
    public function setUseRegister($UseRegister): void
    {
        $this->UseRegister = $UseRegister;
    }


}