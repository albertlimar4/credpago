<?php

namespace Credpago\Model;

use Credpago\DB\Sql;
use Credpago\Model;
use Exception;

class SiteRequest extends Model
{
    private $ReqId;
    private $ReqCode;
    private $ReqRegister;
    private $SitId;

    public function listAllFromSite($nSitId)
    {
        $sql = new Sql();
        return $sql->select("Select * From site_request Where sit_id = :nSitId Order By req_id Desc Limit 5", [
            ":nSitId"=>$nSitId
        ]);
    }

    public function save()
    {
        $sql = new Sql();
        $vResult = $sql->query("Insert Into site_request (req_code, sit_id) Values (:sReqCode, :nSitId)", [
            ":sReqCode"=>$this->getReqCode(),
            ":nSitId"=>$this->getSitId()
        ]);

        if ($vResult[0] == 0)
            return true;
        else
            return false;
    }

    /**
     * @return mixed
     */
    public function getReqId()
    {
        return $this->ReqId;
    }

    /**
     * @param mixed $ReqId
     */
    public function setReqId($ReqId): void
    {
        $this->ReqId = $ReqId;
    }

    /**
     * @return mixed
     */
    public function getReqCode()
    {
        return $this->ReqCode;
    }

    /**
     * @param mixed $ReqCode
     */
    public function setReqCode($ReqCode): void
    {
        $this->ReqCode = $ReqCode;
    }

    /**
     * @return mixed
     */
    public function getReqRegister()
    {
        return $this->ReqRegister;
    }

    /**
     * @param mixed $ReqRegister
     */
    public function setReqRegister($ReqRegister): void
    {
        $this->ReqRegister = $ReqRegister;
    }

    /**
     * @return mixed
     */
    public function getSitId()
    {
        return $this->SitId;
    }

    /**
     * @param mixed $SitId
     */
    public function setSitId($SitId): void
    {
        $this->SitId = $SitId;
    }


}