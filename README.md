# Validação de URLs #

Projeto desenvolvido para utilização em processo seletivo da empresa CREDPAGO. O sistema consiste em 

### Contextualização ###

A Empresa X faz o rastreamento de websites sob demanda. Seus clientes podem acessar a aplicação web para cadastrar as URLs que desejam rastrear. Ao cadastrar uma nova URL o cliente apenas recebe uma confirmação de que a URL foi cadastrada com sucesso, além de poder visualizá-la na sua lista de URLs cadastradas. Dentro de poucos instantes, o robô da Interwebs, que nada mais é do que um script executado separadamente, irá acessar todas as URLs cadastradas, de todos os clientes. O robô irá armazenar o código de status HTTP e o corpo da resposta, de forma que o cliente saiba quando sua URL foi acessada, qual foi o status code retornado, bem como ter a possibilidade de visualizar o corpo do HTML retornado.

### Requisitos ###

* Apache 2.4 ou superior
* PHP 7.3 ou superior
* MySQL 8.0 ou superior
* Slim Framework 2.0
* Rain TPL 3.0

### Instalação do Banco de Dados ###

```
1. Extrair o projeto na pasta htdocs ou similar;
2. Instalar o banco de dados através do arquivo db.sql;
3. Alterar as configurações do banco de dados no arquivo vendor/credpago/src/DB/Sql.php;
```

### Instruções de utilização ###
```
1. Na tela de login clique no botão "Cadastrar";
2. Preencha as informações solicitadas e clique em "Enviar";
3. Faça login para acessar o sistema;
4. Na página inicial clique em "Nova URL" para cadastrar novas;
5. Após o cadastro a nova URL será listada na área inferior da tela;
6. As requisições são atualizadas automaticamente via AJax e Cron;
```