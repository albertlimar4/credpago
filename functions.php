<?php

use Credpago\Model\User;

function getUserName(): string
{
    return maskName($_SESSION[User::SESSION]['UseName']);
}

function maskName($sName): string
{
    $sName = explode(" ", $sName);
    return mb_convert_case("$sName[0] ".end($sName),2,'UTF-8');
}

function validateInput($String): string
{
    $String = trim($String);
    $String = stripslashes($String);
    return htmlspecialchars($String);
}

/**
 * @throws Exception
 */
function formatDate($sDate, $bTime)
{
    if ($sDate) {
        $oData = new DateTime($sDate);
        return ($bTime) ? $oData->format('d/m/Y H:i:s') : $oData->format('d/m/Y');
    } else
        return false;
}